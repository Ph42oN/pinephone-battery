#include <iostream>
#include <fstream>
using namespace std;

string read_status();
int read_charge();

int main(void) {
	int charge = read_charge();
	cout << "{\"percentage\": " << charge
	     << ", \"alt\": \"" << read_status() << "\""
	     << ", \"class\": \"";
	if(charge<15) {
	    cout << "critical";
	} else if(charge<30) {
            cout << "warning";
	}
	cout << "\"}\n";
	return 0;
}

string read_status() {
	ifstream file;
	string line;

	file.open("/sys/class/power_supply/axp20x-battery/status");
	if(!file.is_open()) {
		cerr << "File not found" << endl;
		return 0;
	} else {
        if(getline(file,line)) {
			//return line;
			if (line.compare("Charging") == 0) {
				return "charging";
			} else if (line.compare("Discharging") == 0) {
				return "discharging";
			}
		}
	}
	return "0";
}

int read_charge() {
	ifstream file;
	string line;
	int charge;

	file.open("/sys/class/power_supply/axp20x-battery/capacity");
	if(!file.is_open()) {
		cerr << "File not found" << endl;
		return 0;
	} else {
        if(getline(file,line)) {
			charge = stoi(line);
			return charge;
		}
	}
	return 0;
}
